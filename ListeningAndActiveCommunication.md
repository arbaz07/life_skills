## 1). What are the steps/strategies to do Active Listening?
![Active Listning](https://i2.wp.com/www.autonomouslearner.com/wp-content/uploads/2015/02/Active-Listening-2.png?w=1140&ssl=1)

Active listning is more than just hearing another person words. It is a process where you seeking to unnderstand meaning behind them. Threr are few ways we can become active listner as follows.

+ Active listning requires being fully present in the conversation. it can help you to concentrate and understans on what is being said.
+ Dont use cell phone, ignore distractions. focus on the speeker as there is no one else in the room.
+ It is nessesory to pay attention while you are listning to someone.
+ Try not to interrupt the onther person.
+ Avoid getting distracted.
+ **  Use frases such as. **
     + "Te- ll me more!"
     + "Go ahead, I'm listening".
     + "That sound's interesting".
*  Replaying with body language.
* Take a notes during the IMP conversation if important.

## 2). According to Fisher's model, what are the key points of Reflective Listening?
![reflective listning](https://paulendress.com/wp-content/uploads/2018/07/Reflective-Listening-Steps-640.png)

Reflectiveness is the ability to respond to what they are saying. Following is the summary for reflective listening:
* **Listen To The Speakker's Message** Pay full attention to the speaker and listen to the message he is trying to convey. Be a good listener while listening to the speaker. Take notes on any important points raised by the speaker. 
* **Determine The Meaning Of The Message** Determine the meaning of what the speaker has said or what he meant to say.
* **Reflect Back The Message In Our Own Words**  After the speaker finishes giving the message, you can summarise the message in your own words and reflect it back to the speaker.
* **Seek Confirmation** Whatever you understand about what the speaker is trying to say Ask the speaker if you have understood the message correctly.
This type of communication is widely used.

## 3). What are the obstacles in your listening process?
![image](https://internalchange.com/wp-content/uploads/ECHO-Personal-Listening-Profile.png)
* Distraction is the primary obstacle in listning.
* I find difficulty to make eye contact.
* somethime's i forgot to put mobile on silient mode.
* It can be verry difficult to pay attention when there is sarrounding noise.
* If i am listning to someone and suddenly someone else calls.
* Physical sickness like headache, cold etc.
* Usually i am already thinking about too much things.

## 4). What can you do to improve your listening? 
![image](https://belsmalta.com/wp-content/uploads/2020/08/learn-how-to-listen-better-with-tips-on-esl-listening-skills.jpg)
* I am practising to become good at making eye contact so that I can pay attention and understand the emotions of a speaker.
* keeping my mobile away or on silent if I am talking to someone.
* Focus on the speaker and what he/she is saying.
* Trying not to think about too much things at a time.
* As I am allergic to some environmental conditions,  I usually suffer headaches and colds.

## 5). When do you switch to Passive communication style in your day to day life?
![image](https://experiencelife.lifetime.life/wp-content/uploads/2012/09/Oct12-passive-aggressive.jpg)
* If there is someone close to me and I don't want to affect their feelings in any way by telling them my true expressions, I will choose to communicate with them passively.
* in case of feeling threatened and wanting to get out of a situation using passive communication so that other people's feelings won't be hurt.
* If I know I made a stupid mistake and that the person in front of me will be aggressive, I will use passive communication. 

## 6). When do you switch into Aggressive communication styles in your day to day life?
![](https://image.marriage.com/advice/wp-content/uploads/2019/04/How-to-Deal-with-Angry-Parents.jpg)
* When someone is lying to me about something I already knew, I will use aggressive communication.
* In some cases, if I find that people are taking advantage of me, it is better to have aggressive communication there.
* If a deadline is near and my team is just passing time without thinking about the project or its deadline, at that point I will be speaking to them aggressively.
* If I found myself in a situation where I had to prove my point, it'd be a loss for me, so in order to survive that situation, I'd have to make my point aggressively without thinking about others.

## 7). When do you switch into Passive Aggressive communication styles in your day to day life?

![](https://assets.entrepreneur.com/content/3x2/2000/20160616182014-GettyImages-97595983.jpeg)

* If I am doing a task and the person for whom I am doing it keeps asking me to do it again, even though I did it correctly, my answer to that person would be, "Sure, I'll do it," but under the hood, my intention is not to do that.
* In some cases, I am not 100 percent sure about the answer, but I still have to give it. At that moment, my answer is usually in a passive-aggressive way because I don't know the answer, so it will be like I am saying something that I don't want to say.
* If I feel that someone is crossing my boundaries and I don't like it, I want to let them know that I am not comfortable with it, but I can't tell the truth directly to that person, so I will use a passive-aggressive form of communication.

## 8). How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life?
![](https://trainingconsult.com/wp-content/uploads/2022/04/A-dialog_900.png)

* If I ask someone for something and it is repeatedly ignored, I will try to change the way I ask and ask assertively.
 * understanding how the sentence, which is in an aggressive or passive form, can be arranged in an assertive way so that if I use that, I will be getting the answer that I expected.
 * I can use honesty in my words.
 * I am working to improve my self-confidence. This will help me become a more assertive communicator.
 * When convening others, I have to consider their emotions as well.
 * While speaking, my tone should not be aggressive, this will affect my practise of assertive communication. 














