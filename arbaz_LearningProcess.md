# Learning Process

## 1. What is the Feynman Technique? Paraphrase the video in your own words.


As Einstein said If you can not explain a topic or concept to a child, then you did not understand the concept completely, There is room for improvement.Explaining a concept helps you undestand it. In order to understand a topic conceptually there is a technique called (Fynmens Technique). There are 4 steps in this technique as follows.

![Feynman technique](https://blog.doist.com/wp-content/uploads/2020/02/feynman-technique_graphic-1_resize-1.png)

##### 1). Write down the problem on a piece of paper.
Writing down a concept help you to understand the problem itself. If you don't understand what to learn, It will be verry diffucult to know how to do it.
##### 2). Start Writing the explaination about the concepts
First break the concepts in smaller parts, It will make easy for you to do a research for it, gather information from different sources about the sub-topic's and write it down. Do this for all the sub-topic's you made.
##### 3). Identify any area you are not comfertable in
Now go through the topics again, If you found anything hard to explain go through them or do more research on them.
##### 4). Look for the words difficult to understand.
If you found any difficult to understand or difficult to explain or you think other's would not understand it. Now write down the explaination for them as well In easy to understand language, keep in mind if you can explian It to a child in understandble way you've got it.

## 2. What are the different ways to implement this technique in your learning process?

![Fynmen](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQQjBdmL34ONDzHTpCIPVmV0T12JtIOQXc4wA&usqp=CAU)
As we know Computor science is a dynamic field. Everday there is something new and advance technology upgrading, In order to keep-uo with tech we need to have a effective learning technique. Fynmen's technique for learning will be a great skill to have as a curious learner.
Ther Basics of This technique is that if you can teach a concept effectively then you know it well. Working as a software engineer you should be ready to upgrade your skillset. There are many way we can implement Fynmen's technique as follows.
* Breaking concept into smaller pieces and understanding them one by one.
* Going through a concept repeatedly.
* By teaching to colleagues.

## 3. Paraphrase the video **in detail** in your own words.
Speaker start with speaking about her transformation of her career from serving in army to the poffessor of engineering. Later she explains the techniques to learn something effectively.
Barba Oakley explains the process of learning by dividing her barin into 2 parts **Focused Mode** and **Diffuse Mode**.
- **Focused Mode** when you pay attention on something, avoiding any distractions. But in this case learning happens in preconcieved nations which results in narrow way of learning.
- **Diffuse Mode** In this mode of braing we tend to relax when we got stuck. After relaxing we can start again with fresh mindset and new ideas to solve a problem.

Speaker also said how to avoid procrastination. One way of avoiding it is to continue working without thinking about it. Other way is Promodoro technique in which we have work with full focus for 25 minutes avoiding any distractions. Also Barba Oakley eaxplains the diffrenece between slow learning and fast learning. how slow learning is good using hikers example.

## 4. What are some of the steps that you can take to improve your learning process?
Steps to improve our learning is as follows. We can follow the techniques mentioned by Barba Oakley in order to improve our learning.
*  By practicing Promodoro technique. In this technique we work without any distraction for some time and then took some rest. Doing  as Promodoro technique we allow our brain to take a rest think in relax mode.
* We can Ignore the missconception about the slow learning. Slow learning not bad at all. We ca improve our grasp on a concept as we practice more and more.


## 5. Your key takeaways from the video? Paraphrase your understanding.
- As  Josh Kaufman explains his thougths about the 10,000 hour Rule for learning. I noticed that how information was spreads at each step somebody somehow take diff meaning from it. In 10,000 hour rule was for getting at the top of a verry specific narrow field, But how it was getting promoted as "If you want to learn anything it requires 10000 hours" which is not true.
- Find out how to ddecontruct a skill, because any skill is composed of multiple other skill. by doing that you can start learning about the are nessesory to learn in order to learn.
- Learn how to self-correct. If you made a mistake while learning something. Learn how to correct that mistake.
- Practice is only way to become master in something.




## 6. What are some of the steps that you can take while approaching a new topic?
Approching a new topic there are some topics we can take are as follows
* **Deconstruction of skill**
In order to learn a new skill, first we have to figure out how to break it down into smaller steps of learning skills. By doing that we can improve the amout of time require to learn it.
* **Self-Correction** 
Notice when we make a mistake while doing some task. In order to improve that mistake we have to do something else and try different approch.
* **While learning remove distraction**
In order to focus on learning we have to remove every distraction. By doing that you minimise the time require to aquire that skill.



