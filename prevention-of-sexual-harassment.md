
## What kinds of behaviour cause sexual harassment?

Sexual Harassment is any unwanted visual, physical or verbal conduct, Which can affect working condition or create a Hostile work environmment. There are three main forms of sexual harassment as follows.

**1.Verbal Harassment.**

Verbal harassment is any unwanted verbal comments that makes a reasonable person feel uncomfortable, humiliated, or mentally distressed.

- Commenting negatively about a person’s clothing, body, or personal behavior.
- Sexual jokes.
- Gender based jokes,
- Requesting sexual favour's.
- Asking someone out repeatedly.
- Spreading rumour's about someones personal or sexual life.
- Sending someone sexual emails, notes, or letters
- using bad language.

 
![](https://scmclaw.com/wp-content/uploads/2020/05/sexual-harassment-attorney-orange-county-stevens-mcmillan.jpg)

  

**2.Visual Harassment.**

Visual Harassment is any inappropriate conduct with someones personal belonging's. The most common forms related to visual harassment include:

- Inappropriate Poster's.
- Unwelcome gestures towards another person.
- Inappropriate Drawing's with information related to sexual behaviors
- Inappropriate Cartoon's
- Inappropriate Screen saver's.
- Email or Text of a sexual nature.

  
![](https://mir-s3-cdn-cf.behance.net/projects/404/1d09a480346471.5d0da5d4ab13f.jpg)

  

**3. Physical Harassment.**

Physical harassment is an ac where someone touches another persone against there will. The most common forms related to physical harassment include:

- Sexual assault.
- Inappropriate touching.
- Blocking movements.
- Inappropriate touches to another workers body, hair or clothes.
- Stairing toward a person.
  
![](https://assets.lybrate.com/q_auto:eco,f_auto,w_450/eagle/uploads/2205d15fe15d38f26e2fd5a86aae7697/8c58e6.jpg)

  
  

## 2. What would you do in case you face or witness any incident or repeated incidents of such behaviour?

  

In I witness any kind of harassment including sexual harassment in workplace. I can play an essential role in helping the person targeted by harassment. Different kinds of approches are there In order to help the person.

- **Direct** If I feel that directly addressing harassment is safe and may be effective, I can call out the behavior in the moment. I will let them know that this kind of behavior is inappropriate.

  

- **Distract** we can stop an incident by simply interrupting it. Rather than addressing the aggressor directly we can simply distract them by asking a questions, starting an unrelated conversatioin or by calling out a person of that space.

  

- **Delegate** By finding an appropriate authorised person such as HR, Supervisor or security officer to solve the matter.

  

- **Document** It may be most helpful to document what you are witnessing. If i will record an incident, it can be used as a proof to proove aggressor guilty.
