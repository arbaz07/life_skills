Firewall
========

**Author:** *Arbaz Pathan*

# Abstract

A firewall is a software program or hardware with software program that creates 
a security perimeter whose main function is control unauthorized access of 
incoming and outgoing data or information over a network.

## Firewall

A firewall is essentially the barrier that sits between your network and the public Internet. The main function
of firewall is main function is to control unauthorized access of incoming and outgoing data or information over 
a network. Firewall help us from hackers, viruses that try to reach your computors over the enternate. A firewall
is a software program or a hardware device.

### Software Firewall.

Software firewall is a program which is installed on your computor.

### Hardware Firewall.

Hardware firewall is a device placed between your computor and the public enternate.

## Types of Firewall

### 1. Packet filtering firewall

A small amount of data called packet is analyzed and distributed according to the filter's criteria. 
A packet inspection firewall operates at the network layer of the OSI model. It is are very fast because
there is not much logic going behind the decisions they make. They do not inspect traffic internally, 
they do not store any information. We have to manually open prots for all traffic that will flow through the firewall.

![Packet filtering firewall](https://media.geeksforgeeks.org/wp-content/uploads/1111-6.png)

![Packet filtering firewall](https://media.geeksforgeeks.org/wp-content/uploads/20210609151803/packetfirewall-660x379.png)

### 2. Proxy service.

A proxy firewall acts as a gateway between internal users and the internet.
It is the most secure form of firewall, filtering messages at the application level to protect network resources, 
unauthorized access.
It is also known as application firewall or gateway firewall.

![Proxy Service](https://media.geeksforgeeks.org/wp-content/cdn-uploads/20210910181506/gfg7.png)

### 3. Stateful inspection.

It is also referred to as dynamic packet filtering. A firewall is a technology that monitors the 
state of active connections and uses this information to determine which network packets to allow through the firewall.

![Stateful inspection](https://www.illumio.com/sites/default/files/statefulFWblog_social_1024x512.jpg)

### 4. Next Generation Firewall (NGFW)

NGFWs have more layers of security built into them, to protect against more sophisticated threats.
NGFW’s ability to filter packets based on applications is the basic differnce from traditional firewall.

### 5. circuit-level gateway.

It is a simplified type of firewall that can be easily configured to allow or block traffic without consuming significant computing resources. Circuit-level firewalls are implemented as security software or pre-existing firewalls.

![Stateful inspection](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTdROQBERPZOXCX_CRWTLSVNqL0ApxkrvcSjg&usqp=CAU)

## References

* https://www.youtube.com/watch?v=eO6QKDL3p1I&list=PLBbU9-SUUCwV7Dpk7GI8QDLu3w54TNAA6"
* https://ieeexplore.ieee.org/abstract/document/6088813
* https://www.sciencedirect.com/topics/computer-science/packet-filtering-firewall
* https://www.geeksforgeeks.org/types-of-firewall-and-possible-attacks/
* https://www.fortinet.com/resources/cyberglossary/proxy-firewall
* https://www.techtarget.com/searchnetworking/definition/stateful-inspection
* https://www.geeksforgeeks.org/what-is-stateful-inspection/
* https://docs.moodle.org/400/en/Markdown
