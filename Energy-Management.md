## 1). What are the activities you do that make you relax - Calm quadrant?


* I listen to classical music whenever I'm feeling stressed.
* I close my eyes for a moment and try not to think of anything.
* Go out for a walk.
* Take a shower with cold water.
* Watch comedy videos.
* Games.

## 2). When do you find getting into the Stress quadrant?


* When completing tasks that are difficult to complete. 
* During presentation, there is performance pressure.
* If the topic is not engaging while studying. 
* After watching the motivational video.
* when the submission deadline is closer.


## 3). How do you understand if you are in the Excitement quadrant?

* I feel happy whenever I am excited.
* Spending money without any need.
* When I feel I can do anything
* While opening the gift
* If I am reading something and not getting bored
## 4). Paraphrase the Sleep is your Superpower video in detail.
-  Sleep and rest are the most important factors influencing health.
- When you compare two people, one who sleeps well and the other who does not, you will notice that the ability of the well-sleepers to grasp information is greater than that of the person who does not sleep well.
- During sleep, our mind works on shifting temporary, memorised concepts into permanent ones.
- It is critical to take a nap before starting to learn because it will help you absorb the information, and without sleep, it will act like an inhibitor.
- The hippocampus is in charge of keeping track of human body data. People who sleep well will have a healthy hippocampus, and people who don't sleep well will have a damaged hippocampus.
- Natural killer cells in the human body identify unwanted and dangerous viruses and work to eliminate them; lack of sleep has a negative impact on these cells.
- Sleep has a wide-ranging impact on the human body.
- There should be consistency in your sleep schedule, whether it is a weekday or any other day; your sleep and wake up times should be the same.
- Sleep is not an optional thing in a person's life; it is a non-negotiable biological necessity.


## 5). What are some ideas that you can implement to sleep better?
- 1 minute of meditation before going to bed
- making sure there will be no noise
- Avoid using a cell phone.
- not to overthink before falling asleep.
- avoiding distractions such as television, mobile, and so on.
- keeping lights off.

## 6). Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.

-   Physical activity, which is simply moving your body, has immediate, long-term, and protective benefits for your brain that can last the rest of your life.
-   The prefrontal cortex is part of the brain and is responsible for decision-making, focus, attention, and your personality. The temporal lobes are present on the left and right sides of the brain and are responsible for storing long-term memory in a structure called the hippocampus. The hippocampus is responsible for data processing in the human brain.
-   Exercise or other body-moving activities make the brain stronger and keep it healthy.
-   Better mood, energy, and attention are the aspects that come with exercise with respect to the brain.
-   Exercise has an immediate effect on your mood by increasing levels of neurotransmitters such as dopamine, which leads to an increase in your mood after the exercise, as well as improving focus and reaction time.
-   Exercise changes brain anatomy, physiology, and function.

## 7). What are some steps you can take to exercise more?

- I will follow a weekly exercise pattern.
-  exercise in a set form.
-  I try to drink a little water while exercising.
