## 1). What is your one major takeaway from each one of the 6 sections. So 6 points in total.

#### 1. Gathering requirements

-   Make sure to ask questions and seek clarity in the meeting itself. Since most of the times, it will be difficult to get the same set of people online again

#### 2. Always over-communicate: Some scenarios

-   Use the group chat/channels to communicate most of the time. This is preferable over private DMs simply because there are more eyes on the main channel.


#### 3. Stuck? Ask questions

-   The way you ask a question determines whether it will be answered or not. You need to make it very easy for the person to answer your question. Messages like the database is not connecting, need your help or the build is not working, can you help me out, will not get you the answers you are looking for.
#### 4. Get to know your teammates
-   Join the meetings 5-10 mins early to get some time with your team members
#### 5. Be aware and mindful of other team members

-   It will not always be possible for your team members to get on a call with you. Instead, you can send a slack, Whatsapp message. But instead of bombarding them with many messages, you can write down all the questions you have and send it as a single message

#### 6. Doing things with 100% involvement

-   Work when you work, play when you play is a good thumb rule.

## 2). Which area do you think you need to improve on? What are your ideas to make progress in that area?

- I have to improve my learning style. Being an engineer requires us to be lifelong learners. Every day, there is something new to learn.

- I'm experimenting with techniques like deep learning and focus management to help with this.
