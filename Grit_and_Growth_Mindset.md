## 1). Grit
![image](https://www.choosingwisdom.org/wp-content/uploads/2018/05/Featured-Image-1024x768.jpg)
Grit is a **perseverance and passion for long-term goals**.

when the speaker was teaching math to 7th graders in a New York City public school. She noticed that IQ was not the only factor separating the best and worst students. Some of the best students are not performing well, and some students who got the best results are not the smartest ones. After a few years of teaching, she came to the conclusion that we needed a much better understanding of students and learning from a psychological point of view. So she quit teaching and enrolled in graduate school to become a psychologist. She started studying people in difficult situations to see who would stay and who would be dropped. After conducting research on several different scenarios, one characteristic emerged, which is "**Grit.**"
* Grit is a passion.
* Grit is sticking to your goal for many years.
* Grit is like a long marathon, not a short sprint.

One thing is certain: talent does not make you gritty. If you are not gifted but have clear goals and stick to them, they will come true. To instil grit in children, we must first instil a **growth mindset** in them. The belief that your ability to learn is not fixed and can change with effort is referred to as a growth mindset.

## 2). What are your key takeaways from the video to take action on?
![](https://www.betterup.com/hs-fs/hubfs/Imported_Blog_Media/grit%20matters%20-%20passion%20and%20perseverance-1.png?width=1999&height=1143&name=grit%20matters%20-%20passion%20and%20perseverance-1.png)
* My first key takeaway point is that I can see efforts as necessary if I maintain a growth mindset.
* Sticking to my long-term goals for many years.
* Belief that my ability to learn is not fixed and can change with effort.
* Being consistent in whatever I do.
* Being passionate about my work

## 3). Paraphrase (summarize) the Growth mind-set in a few lines in your own words.
![](https://media.istockphoto.com/id/1185654371/vector/tree-growing-out-of-human-brain-silhouette-illustration-about-growth-mindset-and-good.jpg?s=1024x1024&w=is&k=20&c=sjeFhWtNridRxKzADpcQ7TfXWEnP0DzPJvWxuIm7MT4=)
The speaker talks about how one needs to believe in one’s ability to figure things out on their own. Belief helps in working on consistency. The speaker then discusses the importance of challenging your assumptions about your abilities. The importance of having a long-term curriculum to actually grow in life is a very important thing to have and more important to follow. Last but not least he reminds us to honor our hard work we have put in so far when we eventually have a bad day and face failure, honor the failure to be just part of life learn from it and move on.


## 4). What are your key takeaways from the video to take action on?

* Believe that skill and intelligence are developed and grown. 
* Believe that people are good at something because they worked on it, and others are not good because they haven't done the work.  
* Believe that you are in control of your ability.
* Believe that skills are built.
* We can learn and grow.
* Consider effort to be a useful thing.
* Ready to take on challenges.
* Learn from previous mistakes and see them as a learning opportunity.

## 5). What is the Internal Locus of Control? What is the key point in the video?
![](https://cdn.psychologytoday.com/sites/default/files/styles/image-article_inline_full/public/field_blog_entry_images/2017-08/locus_of_control_joelson.jpg?itok=jDN92Ip7)
* External Locus of Control - Outside factors are outside because they have no control over them, but they can control them because they are intelligent enough to do so.
* Internal locus of control: Who worked hard and spent time on it, and they believe the outcome is the result of their hard work and extra efforts?How much you put into something is something that you have complete control over. 
* To change ourself from an external locus of control to an internal locus of control, we have to start working on things to make them better, and that will give us self-belief.

## 6). Paraphrase (summarize) the video in a few lines in your own words.
![](https://www.mohawkcollege.ca/sites/default/files/styles/gallery_large/public/Alumni/Alumni%20Events/2022%20Growth%20Mindset%20Webinar%20for%20Alumni%20-%20Events%20780x430_0.jpg?itok=fIlDfvGb)
* The first step toward a growth mindset is believing in your ability to figure things out. 
* The second step toward a growth mindset is to question your assumptions. Don't let your assumptions limit or narrow your future vision. 
*  The third step toward a growth mindset is to develop your life curriculum. Don't wait for anything; instead, look into things that will assist you in obtaining that. 
*  The last step toward a growth mindset is to honour the struggle. Difficulties come with more struggle, and struggle makes you stronger. Difficulties are an important aspect of the growth mindset.
## 7). What are your key takeaways from the video to take action on?
* Believing in your ability to figure things out.
* We can develop a growth mindset by questioning our own assumptions.
* By developing our own curriculum.
* By designing our own curriculum for achieving our long-term goal.
* We should not be afraid of failure because, at the end, this failure will teach a lesson.
* By maintaining resilience.
* Honor your struggle.

## 8). What are one or more points that you want to take action on from the manual? (Maximum 3).
* I will stay relaxed and focused no matter what happens.
* I will follow the steps required to solve problems:
	- Relax 
    - Focus - What is the problem? What do I need to know to solve it?
    - Understand - Documentation, Google, Stack Overflow, Github Issues, Internet
    - Code
    - Repeat

* I know more efforts lead to better understanding.
