
# Tiny Habits

A funny clip to show effect of tiny habits: [Jim's altoid prank on Dwight](https://www.youtube.com/watch?v=t4T4oGDW7bM).



## 1). Your takeaways from the video
* Tiny habits are required to achieve a goal.
* Tiny habits multiply the efforts we make to achieve greater results.
* In order to create a habit, one requires motivation, ability, and a trigger.
* We should design tiny habits to get the outcome.

## 2). Your takeaways from the video in as much detail as possible
* **Behaviour = Motivation + Ability + Prompt**
* make very small habits so that it would require very little motivation.
    * Find a behaviour that is easy to do in 30 seconds or less
    * Find the tiniest habit with the biggest impact
* Identifying triggers that are easy to ignore.
	* External TriggersInternal Triggers.
* Starting with something tiny. this habit will naturally grow into big habit

## 3). How can you use B = MAP to make making new habits easier?
* By beginning with a tiny habit because it will require less motivation to begin with.
* 
## 4). Why it is important to "Shine" or Celebrate after each successful completion of habit?

* Self-appreciation and a small celebration are always good motivation.

## 5). Your takeaways from the video (Minimum 5 points)

* There are four stages of Habit Formation  
    1) Noticing
    2)   Wanting
    3) Doing
    4) Liking
* we can't take action if we don't **notice** something.    
* Make bad habits more difficult to achieve, and make good habits easier to achieve.
* If you notice something, you should be curious and **wanting** to take action on it.    
* Two-minute rule. If it takes two minutes, **do** it now.
* Best way to change long term behaviour is with short term feedback
* we must **like** what we do.    
  

## 6). Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?

* The goal of setting goals is to win the game. The goal of system design is to allow us to keep playing the game.
* We must focus on designing systems to achieve those goals. rather than focusing on the goal

## 7). Write about the book's perspective on how to make a good habit easier?
* Keep less distance between you and your good behavior.
* Make it obvious, make it easy to trigger
* We should make good habits satisfying
* Good habits should be easier to achieve.
## 8). Write about the book's perspective on making a bad habit more difficult?
* Increase the distance between yourself and your bad habits.  
* By making it difficult to trigger  
* We must make bad habits unpleasant.  
* make developing bad habits harder.

## 9). Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?
* I should not use social media when I am taking a rest.
* I should keep my mobile phone away from me.
* Keeping the phone turned off

## 10). Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?
* Using a cell phone in bed
* I will keep all the devices that can distract me from taking the rest.
