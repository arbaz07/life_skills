## 1. What is Deep Work?
* Avoiding distactioins.  
* focus deeply on a Hard task for a longer periods of time.
## 2. Paraphrase all the ideas in the above videos and this one **in detail**.
* According to the first video, the optimal time for deep work is more than 45 minutes until you have a focus on work or complete the task.
* In the second video, the speaker tells us that deadlines are good because they give us motivational signals. It also helps to focus on work without any breaks or distractions during working time.
* The speaker discusses the summary of the deep workbook in the third video. We should practise deep work in our day-to-day lives.
* According to Speaker, these are the strategies for implementing deep work.    
    1.  Minimize distractions by scheduling breaks.
    2.  Early morning is the best time to deep work because that time distractions are minimum.   
    3.  Shut down rituals in the evening and create a plan for the next day.

## 3. How can you implement the principles in your day-to-day life?
* By increasing the duration of work day by day.
* Try to make a plan for scheduling distractions.
* Sleep properly during the night.
* Making a plan for the next day before going to sleep
* Make a plan for the next day before going to sleep
* Work in the early morning because our brain is more focused on that time.

## 4 .Your key takeaways from the video
* Social media is not everything.
* Social media affects mental health.
* Spending more time on social media can usually reduce people's productivity.
* Social media is highly addictive, and its influence causes us to be distracted from our work.
